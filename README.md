# Run

```
docker-compose up -d
```

# Usage

Assuming you are in the root of this repository, you can use the kube config like this:

```
export KUBECONFIG=$PWD/kubeconfig.yaml
kubectl get pods -A
```

Instead of in `/var/lib/docker/volumes` data volumes `k3s` generate will be stored in the `data` folder created in the root of the repository for convenient access/management.

# Destroy

```
docker-compose down -v
sudo rm -rf data
```
